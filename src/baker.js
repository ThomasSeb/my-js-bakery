import { Bread } from "./bread";

export class Baker {

    /**
     * 
     * @param {string} name 
     * @param {number} floorSupply 
     */
    constructor(name, floorSupply) {
        this.name = name,
        this.floorSupply = floorSupply
    }
    /**
     * 
     * @param {number} quantity 
     * @param {string} type 
     * @param {number} weight 
     */
    bakeBreads(quantity, type, weight){
        let bread = [];
        
        for (let index = 0; index < quantity && this.floorSupply > 0; index++) {
            let pain = new Bread(type, weight);
            pain.cook(210);
            bread.push(pain);
            this.floorSupply-=1;
            
            
        }return bread;


    }
}


