import { Bread } from "./bread";
import { FrenchPerson } from "./frenchperson";
import { Baker } from "./baker";


let bread = new Bread('campagne', 100);
let firstPerson = new FrenchPerson(100, `Sam`);
let baker = new Baker('Titi', 6);

console.log(bread.cook(250));
console.log(firstPerson.eat(bread));


let stock = baker.bakeBreads(5, 'baguette', 300);

let stockCouronne = baker.bakeBreads(2, 'couronne', 800);

stock = stock.concat(stockCouronne);

console.log(stock);




