/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/baker.js":
/*!**********************!*\
  !*** ./src/baker.js ***!
  \**********************/
/*! exports provided: Baker */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Baker", function() { return Baker; });
/* harmony import */ var _bread__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bread */ "./src/bread.js");


class Baker {

    /**
     * 
     * @param {string} name 
     * @param {number} floorSupply 
     */
    constructor(name, floorSupply) {
        this.name = name,
        this.floorSupply = floorSupply
    }
    /**
     * 
     * @param {number} quantity 
     * @param {string} type 
     * @param {number} weight 
     */
    bakeBreads(quantity, type, weight){
        let bread = [];
        
        for (let index = 0; index < quantity && this.floorSupply > 0; index++) {
            let pain = new _bread__WEBPACK_IMPORTED_MODULE_0__["Bread"](type, weight);
            pain.cook(210);
            bread.push(pain);
            this.floorSupply-=1;
            
            
        }return bread;


    }
}




/***/ }),

/***/ "./src/bread.js":
/*!**********************!*\
  !*** ./src/bread.js ***!
  \**********************/
/*! exports provided: Bread */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Bread", function() { return Bread; });
class Bread {
    /**
     *  
     * @param {string} type 
     * @param {numeric} weight 
     */
    constructor(type, weight){
        this.cooked = false,
        this.type = type,
        this.weight = weight
    }

    /**
     * 
     * @param {number} temp 
     */
    cook(temp){
        if (temp > 200 && temp < 400) {
            this.weight = this.weight*0.9;
            this.cooked = true;
            return true;
        }else {
            return false;
        }

        if (this.cooked === false) {
            console.log(this.cooked);
            
        }

    }
}




/***/ }),

/***/ "./src/frenchperson.js":
/*!*****************************!*\
  !*** ./src/frenchperson.js ***!
  \*****************************/
/*! exports provided: FrenchPerson */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FrenchPerson", function() { return FrenchPerson; });


class FrenchPerson {
    /**
     * 
     * @param {number} hunger 
     * @param {string} name 
     */
    constructor(hunger, name) {
        this.hunger = hunger,
        this.name = name
    }
    /**
     * 
     * @param {instance of Bread} bread 
     */
    eat(bread) {
        if (this.hunger === 100) {
            this.hunger -= bread.weight;
        }
            return this.hunger;

    }
}

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _bread__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bread */ "./src/bread.js");
/* harmony import */ var _frenchperson__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./frenchperson */ "./src/frenchperson.js");
/* harmony import */ var _baker__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./baker */ "./src/baker.js");





let bread = new _bread__WEBPACK_IMPORTED_MODULE_0__["Bread"]('campagne', 100);
let firstPerson = new _frenchperson__WEBPACK_IMPORTED_MODULE_1__["FrenchPerson"](100, `Sam`);
let baker = new _baker__WEBPACK_IMPORTED_MODULE_2__["Baker"]('Titi', 6);

console.log(bread.cook(250));
console.log(firstPerson.eat(bread));


let stock = baker.bakeBreads(5, 'baguette', 300);

let stockCouronne = baker.bakeBreads(2, 'couronne', 800);

stock = stock.concat(stockCouronne);

console.log(stock);






/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2Jha2VyLmpzIiwid2VicGFjazovLy8uL3NyYy9icmVhZC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvZnJlbmNocGVyc29uLmpzIiwid2VicGFjazovLy8uL3NyYy9pbmRleC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxrREFBMEMsZ0NBQWdDO0FBQzFFO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZ0VBQXdELGtCQUFrQjtBQUMxRTtBQUNBLHlEQUFpRCxjQUFjO0FBQy9EOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpREFBeUMsaUNBQWlDO0FBQzFFLHdIQUFnSCxtQkFBbUIsRUFBRTtBQUNySTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOzs7QUFHQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDbEZBO0FBQUE7QUFBQTtBQUFnQzs7QUFFekI7O0FBRVA7QUFDQTtBQUNBLGVBQWUsT0FBTztBQUN0QixlQUFlLE9BQU87QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE9BQU87QUFDdEIsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsT0FBTztBQUN0QjtBQUNBO0FBQ0E7O0FBRUEsMkJBQTJCLDBDQUEwQztBQUNyRSwyQkFBMkIsNENBQUs7QUFDaEM7QUFDQTtBQUNBOzs7QUFHQSxTQUFTOzs7QUFHVDtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7QUFBQTtBQUFPO0FBQ1A7QUFDQTtBQUNBLGVBQWUsT0FBTztBQUN0QixlQUFlLFFBQVE7QUFDdkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxlQUFlLE9BQU87QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0JPO0FBQ1A7QUFDQTtBQUNBLGVBQWUsT0FBTztBQUN0QixlQUFlLE9BQU87QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLGtCQUFrQjtBQUNqQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxDOzs7Ozs7Ozs7Ozs7QUN2QkE7QUFBQTtBQUFBO0FBQUE7QUFBZ0M7QUFDYztBQUNkOzs7QUFHaEMsZ0JBQWdCLDRDQUFLO0FBQ3JCLHNCQUFzQiwwREFBWTtBQUNsQyxnQkFBZ0IsNENBQUs7O0FBRXJCO0FBQ0E7OztBQUdBOztBQUVBOztBQUVBOztBQUVBIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9pbmRleC5qc1wiKTtcbiIsImltcG9ydCB7IEJyZWFkIH0gZnJvbSBcIi4vYnJlYWRcIjtcblxuZXhwb3J0IGNsYXNzIEJha2VyIHtcblxuICAgIC8qKlxuICAgICAqIFxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBuYW1lIFxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBmbG9vclN1cHBseSBcbiAgICAgKi9cbiAgICBjb25zdHJ1Y3RvcihuYW1lLCBmbG9vclN1cHBseSkge1xuICAgICAgICB0aGlzLm5hbWUgPSBuYW1lLFxuICAgICAgICB0aGlzLmZsb29yU3VwcGx5ID0gZmxvb3JTdXBwbHlcbiAgICB9XG4gICAgLyoqXG4gICAgICogXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IHF1YW50aXR5IFxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSB0eXBlIFxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSB3ZWlnaHQgXG4gICAgICovXG4gICAgYmFrZUJyZWFkcyhxdWFudGl0eSwgdHlwZSwgd2VpZ2h0KXtcbiAgICAgICAgbGV0IGJyZWFkID0gW107XG4gICAgICAgIFxuICAgICAgICBmb3IgKGxldCBpbmRleCA9IDA7IGluZGV4IDwgcXVhbnRpdHkgJiYgdGhpcy5mbG9vclN1cHBseSA+IDA7IGluZGV4KyspIHtcbiAgICAgICAgICAgIGxldCBwYWluID0gbmV3IEJyZWFkKHR5cGUsIHdlaWdodCk7XG4gICAgICAgICAgICBwYWluLmNvb2soMjEwKTtcbiAgICAgICAgICAgIGJyZWFkLnB1c2gocGFpbik7XG4gICAgICAgICAgICB0aGlzLmZsb29yU3VwcGx5LT0xO1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgfXJldHVybiBicmVhZDtcblxuXG4gICAgfVxufVxuXG5cbiIsImV4cG9ydCBjbGFzcyBCcmVhZCB7XG4gICAgLyoqXG4gICAgICogIFxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSB0eXBlIFxuICAgICAqIEBwYXJhbSB7bnVtZXJpY30gd2VpZ2h0IFxuICAgICAqL1xuICAgIGNvbnN0cnVjdG9yKHR5cGUsIHdlaWdodCl7XG4gICAgICAgIHRoaXMuY29va2VkID0gZmFsc2UsXG4gICAgICAgIHRoaXMudHlwZSA9IHR5cGUsXG4gICAgICAgIHRoaXMud2VpZ2h0ID0gd2VpZ2h0XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IHRlbXAgXG4gICAgICovXG4gICAgY29vayh0ZW1wKXtcbiAgICAgICAgaWYgKHRlbXAgPiAyMDAgJiYgdGVtcCA8IDQwMCkge1xuICAgICAgICAgICAgdGhpcy53ZWlnaHQgPSB0aGlzLndlaWdodCowLjk7XG4gICAgICAgICAgICB0aGlzLmNvb2tlZCA9IHRydWU7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfWVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMuY29va2VkID09PSBmYWxzZSkge1xuICAgICAgICAgICAgY29uc29sZS5sb2codGhpcy5jb29rZWQpO1xuICAgICAgICAgICAgXG4gICAgICAgIH1cblxuICAgIH1cbn1cblxuXG4iLCJcblxuZXhwb3J0IGNsYXNzIEZyZW5jaFBlcnNvbiB7XG4gICAgLyoqXG4gICAgICogXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IGh1bmdlciBcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gbmFtZSBcbiAgICAgKi9cbiAgICBjb25zdHJ1Y3RvcihodW5nZXIsIG5hbWUpIHtcbiAgICAgICAgdGhpcy5odW5nZXIgPSBodW5nZXIsXG4gICAgICAgIHRoaXMubmFtZSA9IG5hbWVcbiAgICB9XG4gICAgLyoqXG4gICAgICogXG4gICAgICogQHBhcmFtIHtpbnN0YW5jZSBvZiBCcmVhZH0gYnJlYWQgXG4gICAgICovXG4gICAgZWF0KGJyZWFkKSB7XG4gICAgICAgIGlmICh0aGlzLmh1bmdlciA9PT0gMTAwKSB7XG4gICAgICAgICAgICB0aGlzLmh1bmdlciAtPSBicmVhZC53ZWlnaHQ7XG4gICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB0aGlzLmh1bmdlcjtcblxuICAgIH1cbn0iLCJpbXBvcnQgeyBCcmVhZCB9IGZyb20gXCIuL2JyZWFkXCI7XG5pbXBvcnQgeyBGcmVuY2hQZXJzb24gfSBmcm9tIFwiLi9mcmVuY2hwZXJzb25cIjtcbmltcG9ydCB7IEJha2VyIH0gZnJvbSBcIi4vYmFrZXJcIjtcblxuXG5sZXQgYnJlYWQgPSBuZXcgQnJlYWQoJ2NhbXBhZ25lJywgMTAwKTtcbmxldCBmaXJzdFBlcnNvbiA9IG5ldyBGcmVuY2hQZXJzb24oMTAwLCBgU2FtYCk7XG5sZXQgYmFrZXIgPSBuZXcgQmFrZXIoJ1RpdGknLCA2KTtcblxuY29uc29sZS5sb2coYnJlYWQuY29vaygyNTApKTtcbmNvbnNvbGUubG9nKGZpcnN0UGVyc29uLmVhdChicmVhZCkpO1xuXG5cbmxldCBzdG9jayA9IGJha2VyLmJha2VCcmVhZHMoNSwgJ2JhZ3VldHRlJywgMzAwKTtcblxubGV0IHN0b2NrQ291cm9ubmUgPSBiYWtlci5iYWtlQnJlYWRzKDIsICdjb3Vyb25uZScsIDgwMCk7XG5cbnN0b2NrID0gc3RvY2suY29uY2F0KHN0b2NrQ291cm9ubmUpO1xuXG5jb25zb2xlLmxvZyhzdG9jayk7XG5cblxuXG5cbiJdLCJzb3VyY2VSb290IjoiIn0=