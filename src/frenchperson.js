

export class FrenchPerson {
    /**
     * 
     * @param {number} hunger 
     * @param {string} name 
     */
    constructor(hunger, name) {
        this.hunger = hunger,
        this.name = name
    }
    /**
     * 
     * @param {instance of Bread} bread 
     */
    eat(bread) {
        if (this.hunger === 100) {
            this.hunger -= bread.weight;
        }
            return this.hunger;

    }
}