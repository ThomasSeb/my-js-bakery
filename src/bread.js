export class Bread {
    /**
     *  
     * @param {string} type 
     * @param {numeric} weight 
     */
    constructor(type, weight){
        this.cooked = false,
        this.type = type,
        this.weight = weight
    }

    /**
     * 
     * @param {number} temp 
     */
    cook(temp){
        if (temp > 200 && temp < 400) {
            this.weight = this.weight*0.9;
            this.cooked = true;
            return true;
        }else {
            return false;
        }

        if (this.cooked === false) {
            console.log(this.cooked);
            
        }

    }
}


